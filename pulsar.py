#!/usr/bin/env python

'''
Prober of pulsar data

This class creates a pulsar object, allowing
one to probe various pulsar parameters
and export timing residuals.

Attributes:

         get_residuals:   returns pulsar timing residuals
                          as 3 arrays - mjd, res, err

         print_ephemeris: writes current ephemeris
                          to terminal

         print_tim:       Writes TOAs to terminal

         export_tim:      Export TOAs to file

         print_badsats:   Prints excluded SATs to terminal

         export_badsats:  Exports excluded SATS to file

Written by Benjamin Shaw
'''

# Import functions
from __future__ import print_function
import os
import numpy as np
import subprocess
import tempfile
import contextlib


class Pulsar(object):

    '''
    Pulsar object creator
    '''

    def __init__(self, ephemeris, timing_data, sats_del):
        
        """
        Constructor for the Pulsar class. This forms objects 
        corresponding to the ephemeris, tim file and bad MJDs
        file and allows the user to probe their contents whilst
        protecting the original files from writing/accidental
        deletion etc. 
   
        Parameters:
            ephemeris (type=dict)
            timing_data (type=list)
            sats_del (type=list)

        Returns:
            None
        """
 
        self.ephemeris = ephemeris
        self.timing_data = timing_data
        self.sats_del = sats_del

    def get_residuals(self, apply_sats_del=True):
        """
        Forms timing residuals using TEMPO2 and
        returns these as an object.

        Parameters:
             apply_sats_del: (type=bool)
             Boolean that sets whether or 
             not to remove the MJDs from the tim
             file that are contained in the 
             sats.del file. I'll probably remove this
             as we just comment bad TOAs out of the tim
             file directly

        Returns:
             The residuals (type=list)
             This is a 3D list:
                  residuals[0] == MJDs
                  residuals[1] == Residuals [s]
                  residuals[2] == Error [s]
        """

        self._dir = None
        self.apply_sats_del = apply_sats_del
        print("Forming residuals")

        # Switch to the instance's temporary directory, then
        # write the supplied data strings into files
        with self._pushd():
            with open('tim', 'w') as tfile:
                tfile.write('\n'.join(self.timing_data))
            with open('eph', 'w') as tfile:
                for key, value in self.ephemeris.items():
                    line = "{} {}\n".format(key, ' '.join(value))
                    tfile.write(line)
            if self.apply_sats_del == True:
                # Rewrite TIM file without problematic TOAs
                with open('del', 'w') as tfile:
                    try:
                        tfile.write('\n'.join(self.sats_del))
                    except TypeError:
                        tfile.close()
                self._write_filtered()
                self._makeres()
            else:
                self._makeres()

            # TEMPO2 writes out the residuals to a file
            # This loads them into an array where they are processed
            #   and returned as a 3D list
            res = np.loadtxt("out.res", unpack=True, usecols=[0, 5, 6])
            #return mjd, res, err

            res = np.asarray(res)
            res = np.transpose(res)
            res = sorted(res, key=lambda x: x[0])
            res = np.transpose(res)

            return res

    def _write_filtered(self):
        """
        Generates a modified version of the 
        tim file that has the MJDs listed in 
        sats.del removed. 

        Parameters: None

        Returns: None
        """
        # Switch to temp directory:
        with self._pushd():
            sats = np.loadtxt('del', unpack=True, usecols=0)
            with open('filtered', 'a') as out:
                out.write("FORMAT 1\n")
                with open('tim', 'r') as tfile:
                    for line in tfile.readlines():
                        fields = line.split()
                        try:
                            fields[2]
                            if float(fields[2]) in sats:
                                pass
                            else:
                                out.write(line)
                        except IndexError:
                            pass

    def _makeres(self):
        """
        Forms the prefit residuals using TEMPO2

        If apply_sats_del == True then the residuals
          will be formed using the "filtered" tim file
          which does NOT contain MJDS that are listed in 
          the file sats.del.

        Parameters: None

        Returns: None 
        """

        with self._pushd():
            if self.apply_sats_del == True:
                command = ['tempo2-ben', '-output', 'exportres', '-f', 'eph', 'filtered', '-nofit']
            else:
                command = ['tempo2-ben', '-output', 'exportres', '-f', 'eph', 'tim', '-nofit']

        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        proc.wait()


    # This function allows you to use "with _pushd: " to be in a temporary
    # directory, but when the with block exits it will CD back again.
    @contextlib.contextmanager
    def _pushd(self):
        """
        Switch to a temporary directory and 
        proceed to do stuff in that directory and
        then switch back out again. 

        The class writes out the ephemeris, the tim file
        the residuals and (maybe) the filtered tim file
        into this directory, where they are read, written to 
        and processed as appropriate. This avoids us reading
        and writing to the main eph and tim files, avoiding 
        mistakes.

        Parameters: None
        Returns: None
        """
        if self._dir is None:
            self._dir = tempfile.mkdtemp()
            print("Using directory: {}".format(self._dir))

        previous_dir = os.getcwd()
        os.chdir(self._dir)
        yield
        os.chdir(previous_dir)

    def print_ephemeris(self):
        """
        Simple method to print the contents
        of a pulsar's ephemeris to the console.

        Parameters: None
        Returns: None
       
        TO DO: At the moment the ephemeris is printed in 
        a random order as python dictionaries are 
        annoying like that. I'll fix this sometime. 
        In any case the ephemeris can be read into the 
        TEMPO2 in any order. 
        """
        for par, val in self.ephemeris.items():
            print("{} {}".format(par, ' '.join(val)))

    def print_tim(self):
        '''
        Simple method to print the contents
        of a pulsar's tim file to the console.

        Parameters: None
        Returns: None
        '''

        for i in range(0, len(self.timing_data)):
            print(self.timing_data[i])

    def export_tim(self):
        '''
        Exports the contents of the tim file
        to out.tim in the current working directory.
 
        Parameters: None
        Returns: None
        '''
        if os.path.isfile("out.tim"):
            os.remove("out.tim")

        for line in self.timing_data:
            with open("out.tim", 'a') as timout:
                timout.write(line + "\n")

    def print_badsats(self):
        '''
        Simple method that prints the contents of 
        sats.del to the console

        Parameters: None
        Returns: None
        '''
        if self.sats_del:
            for sat in self.sats_del:
                print(sat)

    def export_badsats(self):
        '''
        Exports the contents of sats.del
        to the file out.del in the present working
        directory. If the file out.del already exists, 
        it will be overwritten
        '''

        if os.path.isfile("out.del"):
            os.remove("out.del")

        if self.sats_del:
            for line in self.sats_del:
                with open("out.del", 'a') as delout:
                    delout.write(line + "\n")
        else:
            print("No dels to export")

    def eph(self):
        if self.ephemeris:
           return(self.ephemeris)

    def timfile(self):
        if self.timing_data:
            return(self.timing_data)

    def toa_err_dist(self):
        # To Do: Implement
        pass
