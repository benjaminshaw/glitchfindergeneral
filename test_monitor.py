# Simple tests for predictgp
import os
import pickle
import pytest
import numpy as np
import ska2
import sys

def get_monitor():
    mon_obj = ska2.Monitor()

    return mon_obj

def test_no_alert_three_sigma():
    predres = 0.0
    prederr = 9.999999999999997e-06
    obsres = 1.5632600608533193e-05
    obserr = 9.8e-06
    
    mon = get_monitor()
    status = mon.test_outlier(predres, prederr, obsres, obserr)
    assert status == False

def test_alert_3_sigma():
    predres = 0.0
    prederr = 9.999999999999997e-06
    obsres = -5.8695979606109006e-05
    obserr = 9.8e-06
    mon = get_monitor()
    status = mon.test_outlier(predres, prederr, obsres, obserr)
    assert status == True

def test_no_alert_5_sigma():
    predres = 0.0
    prederr = 9.999999999999997e-06
    obsres = -5.8695979606109006e-05
    obserr = 9.8e-06
    mon = get_monitor()
    status = mon.test_outlier(predres, prederr, obsres, obserr, threshold=5.0)
    assert status == False
