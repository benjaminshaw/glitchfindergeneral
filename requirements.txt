matplotlib==2.2.4
numpy==1.14.3
pytest==3.5.1
scikit_learn==0.22.1
sphinx_rtd_theme==0.4.3
