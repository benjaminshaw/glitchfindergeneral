#!/packages/pulsarsoft/versions/2018-09-28/usr/x86_64_nitrogen/anaconda2/bin/python -u

'''
This call script is a work in progress
'''

#Import modules
from __future__ import print_function
import inotify.adapters
import inotify.calls
import sys, os
import datetime
import ska2

def match_check(timing_data, this_id):
    """
    Checks to see if an archive file is a replacement
    for an existing archive (e.g., the archive might
    have been updated with a new ephemeris) OR the archive
    corresponds to a brand new observations


    Parameters:
        timing_data: Pulsar tim file (type=data)
        this_id:     TOA identifier (type=str)
       

    Returns:
        True if observation has been seen before, 
        else False
    """
    obsids = []
    for line in timing_data:
        fields = line.split(' ')
        if fields[0] != "C" and fields[0] != "FORMAT":
            fullid = fields[0]
        elif fields == "C" and fields[0] != "FORMAT":
            fullid = fields[1]
        else:
            fullid = None
        if fullid is not None:
            obsdate = os.path.splitext(fullid)[0].split('/')[-1].split('_')[0]
            obstime = os.path.splitext(fullid)[0].split('/')[-1].split('_')[1]
            obsid = obsdate + "_" + obstime
            obsids.append(obsid)

    if this_id in obsids:
        return True
    else:
        return False

def autocheck(pulsar):
    """
    Checks the status of auto. auto is a file
    that exists inside each pulsar's directory
    that states whether or not an alert is open
    on that pulsar. If an alert is open the first
    column in the last line will read "OFF". Else 
    it will read "ON".

    Parameters:
        pulsar: The pulsar name (type=str)

    Returns:
        The status of auto ("ON" or "OFF")
        if the auto file is found, else None
 
    """
    auto_path = "/local/scratch/bshaw/SKA/pulsars/" + pulsar + "/auto"
    try:
        with open(auto_path, 'r') as this_auto:
            lines = this_auto.readlines()
            endline = (lines[-1].rstrip('\n')).split()
            return endline[0] 
    except IOError:
        return None
    


def get_list(pulsars):
    """
    Sets list of directories to monitor for
    incoming archive files. These directories 
    will be watched by inotify

    Parameters:
        pulsars: list of pulsars we are monitoring (type=data)

    Returns:
        List of directories where archive files are written (type=list)
    """
    dirs = []
    for pulsar in pulsars:
        arc_path = "/local/scratch/bshaw/SKA/pulsars/" + pulsar + "/archives"
        dirs.append(arc_path)

    return dirs


#def start_listen(dirs):
#    ino = inotify.adapters.Inotify()
#    for thisdir in dirs:
#        try:
#            ino.add_watch(thisdir)
#            print("LISTEN: {}".format(thisdir))
#        except inotify.calls.InotifyError as exc:
#            print("Failed to watch {}: {}".format(thisdir, exc))
#            pass
    
    
def main():
    #Create pulsar database object
    pdb = ska2.PulsarDB()

    # Get list of all pulsars
    pulsars = pdb.get_pulsar_list()

    #Get list of directories to monitor
    dirs = get_list(pulsars)

    #Begin listening
    ino = inotify.adapters.Inotify()
    for thisdir in dirs:
        try:
            ino.add_watch(thisdir)
            print("LISTEN: {}".format(thisdir))
        except inotify.calls.InotifyError as exc:
            print("Failed to watch {}: {}".format(thisdir, exc))
            pass

    #Do things if something happens
    for event in ino.event_gen():
        #print(event)
        if event is not None and event[1] == ['IN_CLOSE_WRITE']:
            
            # Note which directory has changed
            changed = event[2]

            # Name of changed/new file?
            filename = event[3]
            arc_path = changed + "/" + filename

            # Is the file the right extension?
            extension = os.path.splitext(filename)[1]
            if extension == ".FTp":
                # Get name of pulsar
                pulsar = os.path.splitext(filename)[0].split('_')[2]
 
                # Check pulsar is known source
                if pulsar in pulsars:

                    # Get identifier
                    obsdate = os.path.splitext(filename)[0].split('_')[0]
                    #print(obsdate)
                    obstime = os.path.splitext(filename)[0].split('_')[1]
                    #print(obstime)
                    ident = obsdate + "_" +  obstime
                    #print(ident)


                    '''
                    create pulsar object
                    get timing data
                    loop through to see if identifiers match any
                    '''

                    # Create pulsar object
                    this_pulsar = pdb.get_pulsar(pulsar)

                    if match_check(this_pulsar.timfile(), ident) == True:
                        print("Recieved old file: {} - replacing".format(filename))
                        continue

                    # Create timing object
                    timing = ska2.Timer(arc_path, pulsar)
                     
                    # Form TOA
                    timline = timing.calc_toa()

                    # Add TOA to tim file
                    timing.append_tim(timline)

                    # Create monitoring object
                    mon = ska2.Monitor(pulsar)

                    #Proceed based on contents of auto
                    if autocheck(pulsar) == "ON":

                        # Create new pulsar object
                        this_pulsar = pdb.get_pulsar(pulsar)

                        # Get START from ephemeris
                        start = this_pulsar.ephemeris['START'][0]

                        # Generate residuals
                        res = this_pulsar.get_residuals(apply_sats_del=False)

                        # Create GP Object
                        gp = ska2.PredictRes(res, start, showplot=False, poly_n=2) 

                        # Fit GPs using selected covariance functions
                        gp_out = gp.judge()
                        print(gp_out)

                        # Test new data point for outlier status
                        mon.test_outlier(gp_out[0], gp_out[1], gp_out[2], gp_out[3], autostop=True)

                    elif autocheck(pulsar) == "OFF":
                        mon.repeat_alert()
                    else:
                        mon.repeat_alert()

main()
